# MiniSlideShow
![MiniSlideShow](./screenshot.jpg) 

## Description
[MiniSlide Show](http://www.flashyourweb.com/article.php?story=MiniSlideShowReadyforG3) is a flash-based slideshow with support for Gallery 3.  This module will allow you to integrate the slideshow into your existing Gallery theme.  

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
To install, extract the "minislideshow" folder from the zip file into your Gallery 3 modules folder.  Afterwards log into your Gallery web site as an administrator and activate the module in the Admin -> Modules menu. 

Note that this module does not include the actual MiniSlide Show program.  You must download and install it to your web server separately from [here](http://www.flashyourweb.com/staticpages/index.php?page=TheMiniSlideShow).  

After everything is installed, you will have an Admin -> Settings -> MiniSlide Show settings menu option.  Go to this screen to tell Gallery where the MiniSlide Show was installed, and to configure any additional settings.  Afterwards you will have a slideshow link on your album and photo pages that users may click on to view the slideshow.

## History
**Version 1.4.1:**
> - Bug Fix:  Check and see if an album actually has photos in it before displaying the slideshow icon.
> - Released 14 December 2011.
>
> Download: [Version 1.4.1](/uploads/1b7dbf2156570676e8fbba04fa812a4f/minislideshow141.zip)

**Version 1.4.0:**
> - Updated code to insert CSS into Gallery's combined CSS file, instead of loading it as a separate .css file.
> - Added some admin-level "this module requires RSS" messages that get displayed if the RSS module is disabled.
> - Updated the module.info file for recent changes in Gallery.
> - Tested for use with Gallery 3.0.2.
> - Released 08 June 2011.
>
> Download: [Version 1.4.0](/uploads/51b033dda91412c3c356ecd07f19c6f2/minislideshow140.zip)

**Version 1.3.0:**
> - Updated for Gallery 3, RC-1.
> - Released 25 February 2010.
>
> Download: [Version 1.3.0](/uploads/16a53d4ca38012ce3bf12f7fb6c5e5e3/minislideshow130.zip)

**Version 1.2.0:**
> - Tested against Gallery 3, Beta 3.
> - Re-worked some of the code so that theme modifications are no longer necessary.
> - Released 16 September 2009
>
> Download: [Version 1.2.0](/uploads/51678a491df7e8123750493eb45fd2b4/minislideshow120.zip)

**Version 1.1.0:**
> - Added a custom menu icon for MiniSlide Show.
> - Released on 22 August 2009
>
> Download: [Version 1.1.0](/uploads/d95c4043bf7b417c462e12ee121ed19d/minislideshow110.zip)

**Version 1.0.0:**
> - Initial Release
> - Released on 21 August 2009
>
> Download: [Version 1.0.0](/uploads/85a0107f4aca46543cb33bf3a3f4e305/minislideshow100.zip)
